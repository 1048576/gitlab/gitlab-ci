import os
import sys

from collections.abc import Mapping
from collections.abc import Sequence
from typing import Final

from ciutilsclient.abc import CiUtilsClientEnvironment
from ciutilsclient.abc import CiUtilsClientShellOutput
from ciutilsclient.command.buildimage.impl import CiUtilsClientBuildImageCommandImpl
from ciutilsclient.command.registrylogin.impl import CiUtilsClientRegistryLoginCommandImpl
from pygenargnet.abc import NetworkAddressArg
from pygenargnet.impl import NetworkAddressArgImpl
from pygenargpath.abc import DirpathArg
from pygenargpath.abc import FilepathArg
from pygenargpath.impl import PathArgImpl
from pygenargtext.abc import TextArg
from pygenargtext.impl import TextArgImpl
from pygencli.abc import CliArg
from pygencli.abc import CliCommandSequence
from pygencli.abc import CliOption
from pygencli.abc import CliParser
from pygencli.abc import CliRootComplexCmd
from pygencli.abc import CliSimpleCmd
from pygencli.utils import CliUtils
from pygennet.utils import NetworkUtils
from pygenpath.abc import Dirpath
from pygenpath.impl import DirpathImpl
from pygenshell.abc import ShellOutputEntry
from pygenshellclient.abc import ShellClient
from pygentext.utils import TextUtils
from pygentype.abc import ImmutableSequence
from pygentype.err import SystemException


class CiUtilsClientShellOutputImpl(CiUtilsClientShellOutput):
    def write(self, entry: ShellOutputEntry) -> None:
        sys.stdout.write(entry.data)

    def info(self, msg: str) -> None:
        text = TextUtils.format_args(
            tpl="\033[36m{}\033[39m\n",
            args=[
                msg
            ]
        )

        sys.stdout.write(text)


class CiUtilsClientImpl(object):
    CMD_ARG: Final[CliArg] = CliArg("COMMAND")
    BUILD_IMAGE_CMD_NAME: Final[str] = "build-image"
    REGISTRY_LOGIN_CMD_NAME: Final[str] = "registry-login"

    _shell_client: Final[ShellClient]
    _shell_output: Final[CiUtilsClientShellOutput]
    _workspace_dirpath: Final[Dirpath]

    _configdir_option: Final[CliOption]
    _registry_repo_option: Final[CliOption]

    _build_image_kaniko_address_option: CliOption
    _build_image_context_option: CliOption
    _build_image_dockerfile_option: CliOption
    _build_image_target_option: Final[CliOption]

    _registry_login_registry_option: Final[CliOption]
    _registry_login_username_option: Final[CliOption]
    _registry_login_password_option: Final[CliOption]

    _parser: Final[CliParser]

    def __init__(
        self,
        shell_client: ShellClient,
        environment: CiUtilsClientEnvironment
    ) -> None:
        cwd = os.getcwd()

        if (cwd == "/"):
            workspace_dirpath = DirpathImpl.create(cwd)
        else:
            workspace_dirpath = DirpathImpl.create(
                path=TextUtils.format_args(
                    tpl="{}/",
                    args=[
                        cwd
                    ]
                )
            )

        self._shell_client = shell_client
        self._shell_output = CiUtilsClientShellOutputImpl()
        self._workspace_dirpath = workspace_dirpath

        self._configdir_option = CliOption(
            name="configdir",
            arg=CliArg("CIUTILS_CONFIGDIR")
        )
        self._registry_repo_option = CliOption(
            name="registry-repo",
            arg=environment.registry_repo_arg()
        )

        self._build_image_kaniko_address_option = CliOption(
            name="kaniko-address",
            arg=CliArg(
                metavar="CIUTILS_BUILD_IMAGE_KANIKO_ADDRESS"
            )
        )
        self._build_image_context_option = CliOption(
            name="context",
            arg=CliArg(
                metavar="CIUTILS_BUILD_IMAGE_CONTEXT"
            )
        )
        self._build_image_dockerfile_option = CliOption(
            name="dockerfile",
            arg=CliArg(
                metavar="CIUTILS_BUILD_IMAGE_DOCKERFILE"
            )
        )
        self._build_image_target_option = CliOption(
            name="target",
            arg=CliArg("CIUTILS_BUILD_IMAGE_TARGET")
        )

        self._registry_login_registry_option = CliOption(
            name="registry",
            arg=environment.registry_login_registry_arg()
        )
        self._registry_login_username_option = CliOption(
            name="username",
            arg=environment.registry_login_username_arg()
        )
        self._registry_login_password_option = CliOption(
            name="password",
            arg=environment.registry_login_password_arg()
        )

        self._parser = CliUtils.parser(
            command=CliRootComplexCmd(
                arg=self.CMD_ARG,
                options=ImmutableSequence(),
                commands=CliCommandSequence(
                    CliSimpleCmd(
                        name=self.BUILD_IMAGE_CMD_NAME,
                        options=ImmutableSequence(
                            self._configdir_option,
                            self._build_image_kaniko_address_option,
                            self._build_image_context_option,
                            self._build_image_dockerfile_option,
                            self._build_image_target_option,
                            self._registry_repo_option,
                        )
                    ),
                    CliSimpleCmd(
                        name=self.REGISTRY_LOGIN_CMD_NAME,
                        options=ImmutableSequence(
                            self._configdir_option,
                            self._registry_login_registry_option,
                            self._registry_login_username_option,
                            self._registry_login_password_option
                        )
                    )
                )
            )
        )

    def run(self, args: Sequence[str], envs: Mapping[str, str]) -> None:
        self._run(self._parser.parse(args, envs))

    def _run(self, args: Mapping[str, str]) -> None:
        with (CliUtils.value(self.CMD_ARG, args) as value):
            cmd = value

        if (cmd == self.BUILD_IMAGE_CMD_NAME):
            cmd = CiUtilsClientBuildImageCommandImpl(
                workspace_dirpath=self._workspace_dirpath,
                shell_client=self._shell_client,
                shell_output=self._shell_output
            )

            cmd.run(
                configdir=self._read_dirpath_arg(
                    workspace_dirpath=self._workspace_dirpath,
                    args=args,
                    arg=self._configdir_option.arg
                ),
                kaniko_address=self._read_network_address_arg(
                    args=args,
                    arg=self._build_image_kaniko_address_option.arg,
                ),
                context=self._read_dirpath_arg(
                    workspace_dirpath=self._workspace_dirpath,
                    args=args,
                    arg=self._build_image_context_option.arg
                ),
                dockerfile=self._read_filepath_arg(
                    workspace_dirpath=self._workspace_dirpath,
                    args=args,
                    arg=self._build_image_dockerfile_option.arg
                ),
                target=self._read_text_arg(
                    args=args,
                    arg=self._build_image_target_option.arg
                ),
                registry_repo=self._read_text_arg(
                    args=args,
                    arg=self._registry_repo_option.arg
                ),
            )
        elif (cmd == self.REGISTRY_LOGIN_CMD_NAME):
            cmd = CiUtilsClientRegistryLoginCommandImpl(
                shell_output=self._shell_output
            )

            cmd.run(
                configdir=self._read_dirpath_arg(
                    workspace_dirpath=self._workspace_dirpath,
                    args=args,
                    arg=self._configdir_option.arg
                ),
                registry=self._read_text_arg(
                    args=args,
                    arg=self._registry_login_registry_option.arg
                ),
                username=self._read_text_arg(
                    args=args,
                    arg=self._registry_login_username_option.arg
                ),
                password=self._read_text_arg(
                    args=args,
                    arg=self._registry_login_password_option.arg
                )
            )
        else:
            raise SystemException()

    def _read_dirpath_arg(
        self,
        workspace_dirpath: Dirpath,
        args: Mapping[str, str],
        arg: CliArg
    ) -> DirpathArg:
        with (CliUtils.value(arg, args) as value):
            return PathArgImpl(
                description=arg.metavar,
                value=workspace_dirpath.resolve_dirpath(value)
            )

    def _read_filepath_arg(
        self,
        workspace_dirpath: Dirpath,
        args: Mapping[str, str],
        arg: CliArg
    ) -> FilepathArg:
        with (CliUtils.value(arg, args) as value):
            return PathArgImpl(
                description=arg.metavar,
                value=workspace_dirpath.resolve_filepath(value)
            )

    def _read_text_arg(self, args: Mapping[str, str], arg: CliArg) -> TextArg:
        with (CliUtils.value(arg, args) as value):
            return TextArgImpl.create_required(
                description=arg.metavar,
                value=value
            )

    def _read_network_address_arg(
        self,
        args: Mapping[str, str],
        arg: CliArg
    ) -> NetworkAddressArg:
        with (CliUtils.value(arg, args) as value):
            return NetworkAddressArgImpl(
                description=arg.metavar,
                value=NetworkUtils.create(value)
            )
