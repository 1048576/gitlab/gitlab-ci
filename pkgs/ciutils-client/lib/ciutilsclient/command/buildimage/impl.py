from typing import Final

from ciutilsclient.abc import CiUtilsClientShellOutput
from ciutilsclient.command.buildimage.abc import CiUtilsClientBuildImageCommand
from ciutilsregistry.utils import CiUtilsRegistryUtils
from pygenargnet.abc import NetworkAddressArg
from pygenargpath.abc import DirpathArg
from pygenargpath.abc import FilepathArg
from pygenargtext.abc import TextArg
from pygenargtext.impl import TextArgImpl
from pygenpath.abc import Dirpath
from pygenshellclient.abc import ShellClient
from pygentext.utils import TextUtils
from pygentype.abc import FinalSequence
from pygentype.abc import ImmutableSequence


class CiUtilsClientBuildImageCommandImpl(CiUtilsClientBuildImageCommand):
    _workspace_dirpath: Final[Dirpath]
    _shell_client: Final[ShellClient]
    _shell_output: Final[CiUtilsClientShellOutput]

    def __init__(
        self,
        workspace_dirpath: Dirpath,
        shell_client: ShellClient,
        shell_output: CiUtilsClientShellOutput
    ) -> None:
        self._workspace_dirpath = workspace_dirpath
        self._shell_client = shell_client
        self._shell_output = shell_output

    def run(
        self,
        configdir: DirpathArg,
        kaniko_address: NetworkAddressArg,
        context: DirpathArg,
        dockerfile: FilepathArg,
        target: TextArg,
        registry_repo: TextArg
    ) -> None:
        cache_repo = TextArgImpl.create_required(
            description="Cache repo",
            value=TextUtils.format_args(
                tpl="{}/cache",
                args=[
                    registry_repo.value()
                ]
            )
        )

        call_args = ImmutableSequence(
            "/kaniko/executor",
            "--context",
            context.value().path(),
            "--dockerfile",
            dockerfile.value().path(),
            "--target",
            target.value(),
            "--skip-unused-stages",
            "--no-push",
            "--cache",
            "--cache-repo",
            cache_repo.value()
        )
        exec = TextArgImpl.create_required(
            description="Exec",
            value=TextUtils.join(" ", call_args)
        )

        self._shell_output.info(kaniko_address.describe())
        self._shell_output.info(context.describe())
        self._shell_output.info(dockerfile.describe())
        self._shell_output.info(target.describe())
        self._shell_output.info(cache_repo.describe())
        self._shell_output.info(exec.describe())
        self._shell_output.info("Build log:")

        with (self._shell_client.connect(kaniko_address.value()) as session):
            session.call(
                args=ImmutableSequence(
                    "upload",
                    "/kaniko/.docker/config.json",
                    CiUtilsRegistryUtils.cfg_to_text(
                        cfg=CiUtilsRegistryUtils.load_cfg(configdir.value())
                    )
                ),
                output=self._shell_output
            )

            session.call(
                args=call_args,
                output=self._shell_output
            )


__all__: FinalSequence[str] = [
    "CiUtilsClientBuildImageCommandImpl"
]
