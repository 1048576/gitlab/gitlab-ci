from pygenargpath.abc import DirpathArg
from pygenargtext.abc import TextArg
from pygentype.abc import FinalSequence
from pygentype.err import SystemException


class CiUtilsClientRegistryLoginCommand(object):
    def run(
        self,
        configdir: DirpathArg,
        registry: TextArg,
        username: TextArg,
        password: TextArg
    ) -> None:
        raise SystemException()


__all__: FinalSequence[str] = [
    "CiUtilsClientRegistryLoginCommand"
]
