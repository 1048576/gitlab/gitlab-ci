#!/usr/bin/env python

import os

from collections.abc import Sequence

import setuptools


def read_requirements() -> Sequence[str]:
    with (open("requirements.txt", "r") as f):
        return f.read().splitlines()


setuptools.setup(
    name="ciutils-client",
    version=os.environ.get("BUILD_ARG_VERSION", "0.0.dev"),
    author="Vladyslav Kazakov",
    author_email="kazakov1048576@gmail.com",
    url="https://gitlab.com/1048576/ciutils.d/ciutils",
    install_requires=read_requirements(),
    package_data={
        "ciutilsclient": [
            "py.typed"
        ]
    },
    package_dir={
        "": "lib"
    },
    packages=setuptools.find_packages("./lib/"),
    entry_points={},
    scripts=[],
    license="MIT"
)
