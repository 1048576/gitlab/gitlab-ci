from typing import NamedTuple

from pygentype.abc import Dict
from pygentype.abc import FinalSequence


class CiUtilsRegistryAuthCfg(NamedTuple):
    username: object
    password: object


class CiUtilsRegistryCfg(NamedTuple):
    auths: Dict[str, CiUtilsRegistryAuthCfg]


__all__: FinalSequence[str] = [
    "CiUtilsRegistryCfg"
]
