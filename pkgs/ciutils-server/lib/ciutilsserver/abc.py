from collections.abc import Mapping
from collections.abc import Sequence

from pygentype.abc import FinalSequence
from pygentype.err import SystemException


class CiUtilsServer(object):
    def serve_forever(self) -> None:
        raise SystemException()


class CiUtilsServerRunner(object):
    def create(
        self,
        args: Sequence[str],
        envs: Mapping[str, str]
    ) -> CiUtilsServer:
        raise SystemException()


__all__: FinalSequence[str] = [
    "CiUtilsServer",
    "CiUtilsServerRunner"
]
