from typing import Dict
from typing import Iterable
from typing import Tuple


class Storage:
    def pipeline_id(self) -> str: ...
    def git_server_host(self) -> str: ...
    def workspace(self) -> str: ...
    def registry_image(self) -> str: ...
    def build_args(self) -> Iterable[Tuple[str, str]]: ...


class StorageFactory:
    def create(
        self,
        args: Dict[str, str],
        git_server_host: str,
        workspace: str
    ) -> Storage: ...

    def attach(self, args: Dict[str, str]) -> Storage: ...
