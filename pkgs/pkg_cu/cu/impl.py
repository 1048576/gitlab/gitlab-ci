from typing import Dict
from typing import List

from cu.abstract import CiUtils
from cu.command.abstract import Command
from cu.command.deploy.kube.impl import DeployKubeCommandImpl
from cu.command.deploy.kube.pipiline.abstract import DeployKubePipeline
from cu.pipeline.abstract import Pipeline
from cu.pipeline.abstract import PipelineFactory
from pygenlog.logger.abstract import Logger
from pygenshell.remote.abstract import RemoteShell


class CiUtilsImpl(CiUtils):
    _pipeline_factory: PipelineFactory
    _shell: RemoteShell
    _logger: Logger

    def __init__(
        self,
        pipeline_factory: PipelineFactory,
        shell: RemoteShell,
        logger: Logger
    ) -> None:
        self._pipeline_factory = pipeline_factory
        self._shell = shell
        self._logger = logger

    def run(self, argv: List[str], envs: Dict[str, str]) -> None:
        pipeline = self._pipeline_factory.create_pipeline(argv, envs)
        command = self._create_command(pipeline)

        command.run()

    def _create_command(self, pipeline: Pipeline) -> Command:
        if (isinstance(pipeline, DeployKubePipeline)):
            return DeployKubeCommandImpl(
                shell=self._shell,
                logger=self._logger,
                pipeline=pipeline
            )

        return Command()
