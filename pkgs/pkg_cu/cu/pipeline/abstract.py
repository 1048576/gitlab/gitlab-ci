from typing import Dict
from typing import List


class Pipeline:
    ...


class PipelineFactory:
    def create_pipeline(
        self,
        argv: List[str],
        envs: Dict[str, str]
    ) -> Pipeline:
        ...
