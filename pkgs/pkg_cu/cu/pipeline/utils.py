from typing import Dict

from pygenarg.address.abstract import AddressArg
from pygenarg.address.impl import AddressArgImpl
from pygenarg.path.abstract import PathResolverArg
from pygenarg.path.impl import PathResolverArgImpl
from pygenarg.text.abstract import TextArg
from pygenarg.text.impl import TextArgImpl
from pygenpath.abstract import PathResolver
from pygentype.address.abstract import Address


def env_http_proxy(envs: Dict[str, str]) -> TextArg:
    return TextArgImpl.create_env_optional(
        envs=envs,
        env_name="http_proxy"
    )


def env_https_proxy(envs: Dict[str, str]) -> TextArg:
    return TextArgImpl.create_env_optional(
        envs=envs,
        env_name="https_proxy"
    )


def env_no_proxy(envs: Dict[str, str]) -> TextArg:
    return TextArgImpl.create_env_optional(
        envs=envs,
        env_name="no_proxy"
    )


def http_proxy(value: str) -> TextArg:
    return TextArgImpl.create_text_optional(
        description="http_proxy",
        value=value
    )


def workdir(path_resolver: PathResolver) -> PathResolverArg:
    return PathResolverArgImpl(
        description="Workdir",
        value=path_resolver
    )


def pipeline_id(value: str) -> TextArg:
    return TextArgImpl.create_text_required(
        description="Pipeline",
        value=value
    )


def https_proxy(value: str) -> TextArg:
    return TextArgImpl.create_text_optional(
        description="https_proxy",
        value=value
    )


def no_proxy(value: str) -> TextArg:
    return TextArgImpl.create_text_optional(
        description="no_proxy",
        value=value
    )


def service_address(value: Address) -> AddressArg:
    return AddressArgImpl(
        description="Service address",
        value=value
    )


def app_version(value: str) -> TextArg:
    return TextArgImpl.create_text_required(
        description="App version",
        value=value
    )
