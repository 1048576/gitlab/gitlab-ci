from argparse import Namespace

from cu.core.arg.parser import ArgParser
from cu.core.arg.parser import ArgPositionalArgument
from cu.core.errors import ExpectedException
from cu.core.logger import Logger
from cu.storage.impl import CiUtilsStorageFactory


class VersionSetCommand():
    storage_factory: CiUtilsStorageFactory

    arg_new_version: ArgPositionalArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.arg_new_version = parser.add_positional_argument(
            metavar="NEW-VERSION",
            help="New version value"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        namespace = storage.get_namespace()

        if (namespace not in ("feature", "test")):
            raise ExpectedException("Unsupported namespace [%s]", [namespace])

        new_version = self.arg_new_version.value(ns)
        storage.set_version(new_version)
        logger.info("New version: [%s]", [new_version])
