from argparse import Namespace

from cu.cli_cmd import CliCommand
from cu.command.version.next_patch_cmd import VersionNextPatchCommand
from cu.command.version.set_cmd import VersionSetCommand
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger


class VersionCommand(CliCommand):
    arg_cmd: ArgPositionalArgument
    cmd_set: VersionSetCommand
    cmd_next_patch: VersionNextPatchCommand

    def __init__(self, parser: ArgParser) -> None:
        self.arg_cmd, subparsers = parser.add_subparsers(
            metavar="VERSION-COMMAND"
        )

        self.cmd_set = VersionSetCommand(
            subparsers.add_parser(
                name="set",
                help="Set new version"
            )
        )

        self.cmd_next_patch = VersionNextPatchCommand(
            subparsers.add_parser(
                name="next-patch",
                help="Calculate next patch"
            )
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        version_command = self.arg_cmd.value(ns)
        if (version_command == "set"):
            self.cmd_set.run(logger, ns)
        elif (version_command == "next-patch"):
            self.cmd_next_patch.run(logger, ns)
        else:
            raise ExpectedException(
                "Unknown version command [%s]" % version_command
            )
