import re

from argparse import Namespace

from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.parser import ArgParser
from cu.core.command import Command
from cu.core.errors import ExpectedException
from cu.core.git import Git
from cu.core.logger import Logger
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorageFactory


class VersionNextPatchCommand():
    storage_factory: CiUtilsStorageFactory

    flag_ssh_private_key: ArgFlagArgument
    flag_ssh_known_hosts: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.flag_ssh_private_key = parser.add_flag_argument(
            flag_name="ssh-private-key",
            env_name="CU_SSH_PRIVATE_KEY"
        )
        self.flag_ssh_known_hosts = parser.add_flag_argument(
            flag_name="ssh-known-hosts",
            env_name="CU_SSH_KNOWN_HOSTS"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        namespace = storage.get_namespace()
        workspace = storage.get_workspace()
        try:
            validator = Validators.re_validator_simplified(
                r"release(?:\-.+)?-(?:0|[1-9][0-9]*)\.(?:0|[1-9][0-9]*)"
            )
            branch = validator.verify(storage.get_branch())
        except ExpectedException as e:
            raise e.wrap("Invalid branch")
        git = Git.create(
            git_server_host=storage.get_git_server_host(),
            project_path=storage.get_project_path(),
            flag_ssh_private_key=self.flag_ssh_private_key,
            flag_ssh_known_hosts=self.flag_ssh_known_hosts,
            ns=ns
        )

        if (namespace != "rc"):
            raise ExpectedException("Unsupported namespace [%s]", [namespace])

        logger.info("Namespace: [%s]", [namespace])
        logger.info("Workspace: [%s]", [workspace])
        logger.info("Commit branch: [%s]", [branch])
        logger.info("Git repo: [%s]", [git.repo])
        logger.info("Private key: [%s]", [git.ssh_private_key])
        logger.info("Known hosts: [%s]", [git.ssh_known_hosts])

        xy = branch[len("release-"):]
        last_patch = -1

        tags_cmd = Command.local(
            "git",
            "-C",
            workspace,
            "ls-remote",
            "--tags",
            git.repo,
            "v%s.*" % xy
        )
        tags = tags_cmd.call_output(
            logger=logger,
            env=git.env
        )
        if (tags != ""):
            pattern = re.compile(r"[a-f0-9]+\trefs\/tags\/v%s\.(?P<patch>0|[1-9][0-9]*)\-(?:rc|stable)" % xy)  # noqa: E501
            for line in tags.split("\n"):
                matched = pattern.match(line)
                if (matched is not None):
                    patch = int(matched.group("patch"))
                    if (last_patch < patch):
                        last_patch = patch

        new_version = "%s.%d" % (xy, last_patch + 1)
        storage.set_version(new_version)
        logger.info("New version: [%s]", [new_version])
