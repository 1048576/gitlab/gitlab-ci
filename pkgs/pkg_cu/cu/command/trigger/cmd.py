from argparse import Namespace

from cu.cli_cmd import CliCommand
from cu.command.trigger.call_cmd import TriggerCallCommand
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger


class TriggerCommand(CliCommand):
    arg_cmd: ArgPositionalArgument
    cmd_call: TriggerCallCommand

    def __init__(self, parser: ArgParser) -> None:
        self.arg_cmd, subparsers = parser.add_subparsers(
            metavar="TRIGGER-COMMAND"
        )

        self.cmd_call = TriggerCallCommand(
            subparsers.add_parser(
                name="call",
                help="Call trigger"
            )
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        trigger_command = self.arg_cmd.value(ns)
        if (trigger_command == "call"):
            self.cmd_call.run(logger, ns)
        else:
            raise ExpectedException(
                "Unknown trigger command [%s]" % trigger_command
            )
