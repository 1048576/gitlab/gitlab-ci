import os

from argparse import Namespace

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorageFactory


class PipelineCreateCommand:
    storage_factory: CiUtilsStorageFactory

    arg_namespace: ArgPositionalArgument

    flag_workspace: ArgFlagArgument
    flag_git_server_host: ArgFlagArgument
    flag_project_path: ArgFlagArgument
    flag_registry_image: ArgFlagArgument
    flag_branch: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.arg_namespace = parser.add_positional_argument(
            metavar="NAMESPACE",
            help="Namespace"
        )

        self.flag_workspace = parser.add_flag_argument(
            flag_name="workspace",
            env_name="CU_WORKSPACE"
        )
        self.flag_git_server_host = parser.add_flag_argument(
            flag_name="git-server-host",
            env_name="CI_SERVER_HOST"
        )
        self.flag_project_path = parser.add_flag_argument(
            flag_name="project-path",
            env_name="CI_PROJECT_PATH"
        )
        self.flag_registry_image = parser.add_flag_argument(
            flag_name="registry-image",
            env_name="CI_REGISTRY_IMAGE"
        )
        self.flag_branch = parser.add_flag_argument(
            flag_name="branch",
            env_name="CI_COMMIT_BRANCH"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        namespace = self.fetch_namespace(ns)
        workspace = os.path.abspath(self.fetch_workspace(ns)) + "/"
        git_server_host = self.fetch_git_server_host(ns)
        project_path = self.fetch_project_path(ns)
        registry_image = self.fetch_registry_image(ns)
        branch = self.fetch_branch(ns)

        storage.set_namespace(namespace)
        logger.info("Namespace: [%s]", [namespace])

        storage.set_workspace(workspace)
        logger.info("Workspace: [%s]", [workspace])

        storage.set_git_server_host(git_server_host)
        logger.info("Git server host: [%s]", [git_server_host])

        storage.set_project_path(project_path)
        logger.info("Project path: [%s]", [project_path])

        storage.set_registry_image(registry_image)
        logger.info("Registry image: [%s]", [registry_image])

        storage.set_branch(branch)
        logger.info("Commit branch: [%s]", [branch])

    def fetch_namespace(self, ns: Namespace) -> str:
        namespace = self.arg_namespace.value(ns)

        if (namespace not in ("feature", "test", "rc")):
            raise ExpectedException("Unsupported namespace [%s]", [namespace])

        return namespace

    def fetch_workspace(self, ns: Namespace) -> str:
        if self.flag_workspace.is_defined(ns):
            validator = ArgArgumentValidator(Validators.dir_path_validator())

            return validator.verify(self.flag_workspace, ns)
        else:
            return os.getcwd()

    def fetch_git_server_host(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_git_server_host, ns)

    def fetch_project_path(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_project_path, ns)

    def fetch_registry_image(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_registry_image, ns)

    def fetch_branch(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return validator.verify(self.flag_branch, ns)
