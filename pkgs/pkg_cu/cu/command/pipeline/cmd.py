from argparse import Namespace

from cu.cli_cmd import CliCommand
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger

from .create_cmd import PipelineCreateCommand
from .fetch_args_from_branch_cmd import FetchArgsFromBranchCommand
from .snapshot_cmd import PipelineSnapshotCommand


class PipelineCommand(CliCommand):
    arg_cmd: ArgPositionalArgument
    cmd_create: PipelineCreateCommand
    cmd_snapshot: PipelineSnapshotCommand
    cmd_fetch_args_from_branch: FetchArgsFromBranchCommand

    def __init__(self, parser: ArgParser) -> None:
        self.arg_cmd, subparsers = parser.add_subparsers(
            metavar="PIPELINE-COMMAND"
        )

        self.cmd_create = PipelineCreateCommand(
            subparsers.add_parser(
                name="create",
                help="Create pipeline"
            )
        )

        self.cmd_snapshot = PipelineSnapshotCommand(
            subparsers.add_parser(
                name="snapshot",
                help="Create snapshot"
            )
        )

        self.cmd_fetch_args_from_branch = FetchArgsFromBranchCommand(
            subparsers.add_parser(
                name="fetch-args-from-branch",
                help="Use part(s) of branch name as arg(s)"
            )
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        pipeline_command = self.arg_cmd.value(ns)
        if (pipeline_command == "create"):
            self.cmd_create.run(logger, ns)
        elif (pipeline_command == "snapshot"):
            self.cmd_snapshot.run(logger, ns)
        elif (pipeline_command == "fetch-args-from-branch"):
            self.cmd_fetch_args_from_branch.run(logger, ns)
        else:
            raise ExpectedException(
                "Unknown pipeline command [%s]" % pipeline_command
            )
