import json

from typing import List

from cu.command.abstract import Command
from cu.command.build.image.pipeline.abstract import BuildImagePipeline
from pygenarg.text.impl import TextArgImpl
from pygenlog.logger.abstract import Logger
from pygenshell.remote.abstract import RemoteShell
from pygenshell.std.stream.adapter.impl import ShellStdStreamAdapterImpl


class BuildImageCommandImpl(Command):
    _shell: RemoteShell
    _logger: Logger
    _pipeline: BuildImagePipeline

    def __init__(
        self,
        shell: RemoteShell,
        logger: Logger,
        pipeline: BuildImagePipeline
    ) -> None:
        self._shell = shell
        self._logger = logger
        self._pipeline = pipeline

    def run(self) -> None:
        pipeline_id = self._pipeline.pipeline_id()
        service_address = self._pipeline.service_address()
        context = self._pipeline.context()
        dockerfile = self._pipeline.dockerfile()
        target = self._pipeline.target()
        registry_image = self._pipeline.registry_image()
        registry_username = self._pipeline.registry_username()
        registry_password = self._pipeline.registry_password()

        cache_repo = TextArgImpl.create_text_required(
            description="Cache repo",
            value="{}/cache".format(registry_image.value())
        )

        tag = TextArgImpl.create_text_required(
            description="Tag",
            value="{}/tmp:{}-{}".format(
                registry_image.value(),
                target.value(),
                pipeline_id.value()
            )
        )

        docker_config = json.dumps(
            obj={
                "auths": {
                    registry_image.value(): {
                        "username": registry_username.value(),
                        "password": registry_password.value()
                    }
                }
            },
            separators=(",", ":"),
            sort_keys=True
        )

        service_address.describe(self._logger)
        context.describe(self._logger)
        dockerfile.describe(self._logger)
        target.describe(self._logger)
        cache_repo.describe(self._logger)
        tag.describe(self._logger)
        registry_username.describe(self._logger)

        create_config_call_args: List[str] = [
            "echo",
            "'{}'".format(docker_config),
            ">",
            "/kaniko/.docker/config.json"
        ]

        self._shell.call(
            address=service_address.value(),
            args=create_config_call_args,
            stdout=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            ),
            stderr=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            )
        )

        build_image_call_args: List[str] = [
            "/kaniko/executor",
            "--skip-unused-stages",
            "--target",
            target.value(),
            "--context",
            context.value(),
            "--dockerfile",
            dockerfile.value(),
            "--cache",
            "--cache-repo",
            cache_repo.value(),
            "--destination",
            tag.value()
        ]

        self._shell.call(
            address=service_address.value(),
            args=build_image_call_args,
            stdout=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            ),
            stderr=ShellStdStreamAdapterImpl.create_unclassified_logger(
                logger=self._logger
            )
        )
