from cu.pipeline.abstract import Pipeline
from pygenarg.address.abstract import AddressArg
from pygenarg.path.abstract import PathResolverArg
from pygenarg.text.abstract import TextArg


class BuildImagePipeline(Pipeline):
    def pipeline_id(self) -> TextArg:
        ...

    def workdir(self) -> PathResolverArg:
        ...

    def service_address(self) -> AddressArg:
        ...

    def context(self) -> TextArg:
        ...

    def dockerfile(self) -> TextArg:
        ...

    def target(self) -> TextArg:
        ...

    def registry_image(self) -> TextArg:
        ...

    def registry_username(self) -> TextArg:
        ...

    def registry_password(self) -> TextArg:
        ...
