import os

from argparse import Namespace
from typing import Dict

import cu.core.path_utils as path_utils

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.argument import ArgMultiArgumentValidator
from cu.core.arg.argument import ArgMultiFlagArgument
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorage
from cu.storage.impl import CiUtilsStorageFactory
from jinja2 import Template


class JinjaRenderCommand():
    storage_factory: CiUtilsStorageFactory

    flag_template_path: ArgFlagArgument
    arg_destination_path: ArgPositionalArgument
    flag_args: ArgMultiFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.flag_template_path = parser.add_flag_argument(
            flag_name="template",
            env_name="CU_JINJA_TEMPLATE",
            help="Template file path"
        )
        self.arg_destination_path = parser.add_positional_argument(
            metavar="DESTINATION",
            help="Destination file path"
        )
        self.flag_args = parser.add_multi_flag_argument(
            flag_name="arg",
            metavar="ARG"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        workspace = storage.get_workspace()
        template_path = self.fetch_template_path(ns, workspace)
        destination_path = self.fetch_destination_path(
            ns=ns,
            workspace=workspace,
            template_path=template_path
        )
        args = self.fetch_args(storage, ns)

        logger.info("Workspace: [%s]", [workspace])
        logger.info("Template: [%s]", [template_path])
        logger.info("Destination: [%s]", [destination_path])
        for k, v in sorted(args.items()):
            logger.info("Arg [%s]: [%s]", [k, v])

        try:
            with open(template_path, "r") as f:
                template = f.read()
        except Exception as e:
            raise ExpectedException.create_and_wrap(
                msg_template=str(e),
                title_template="Cannot read template file [%s]",
                title_vars=[template_path]
            )

        try:
            parsed_template = Template(template, keep_trailing_newline=True)
        except Exception as e:
            raise ExpectedException.create_and_wrap(
                msg_template=str(e),
                title_template="Invalid template [%s]",
                title_vars=[template_path]
            )

        content: str = parsed_template.render(args)  # type: ignore

        os.makedirs(os.path.dirname(destination_path), exist_ok=True)

        try:
            with open(destination_path, "w") as f:
                f.write(content)
        except Exception as e:
            raise ExpectedException.create_and_wrap(
                msg_template=str(e),
                title_template="Cannot write to file [%s]",
                title_vars=[destination_path]
            )

    def fetch_template_path(self, ns: Namespace, workspace: str) -> str:
        validator = ArgArgumentValidator(
            Validators.file_path_validator()
        )

        path = validator.verify(self.flag_template_path, ns)

        return path_utils.abs(path, workspace)

    def fetch_destination_path(
        self,
        ns: Namespace,
        workspace: str,
        template_path: str
    ) -> str:
        validator = ArgArgumentValidator(
            Validators.path_validator()
        )

        dst = validator.verify(self.arg_destination_path, ns)

        if template_path.endswith(".j2"):
            src = template_path[:-len(".j2")]
        else:
            src = template_path

        return path_utils.abs_src_dst(src, dst, workspace)[1]

    def fetch_args(
        self,
        storage: CiUtilsStorage,
        ns: Namespace
    ) -> Dict[str, str]:
        validator = ArgMultiArgumentValidator(
            Validators.re_validator(r"[^\=]+(?:\=[^\=]+)?")
        )

        args: Dict[str, str] = {}

        for pair in validator.verify(self.flag_args, ns):
            items = pair.split("=")
            if (len(items) == 1):
                k = items[0]
                v = os.environ.get(k, "")
            else:
                k, v = items
            args[k] = v

        return {
            **storage.get_args(),
            **args
        }
