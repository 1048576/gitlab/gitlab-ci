from argparse import Namespace

from cu.core.arg.parser import ArgParser
from cu.core.artifact import Artifact
from cu.core.logger import Logger
from cu.core.registry import Config
from cu.storage.impl import CiUtilsStorageFactory


class ImagePushCommand():
    storage_factory: CiUtilsStorageFactory

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        namespace = storage.get_namespace()
        version = storage.get_version()
        config = Config()
        registry_image = config.create_registry_image(
            registry_image_name=storage.get_registry_image()
        )

        logger.info("Namespace: [%s]", [namespace])
        logger.info("Version: [%s]", [version])
        logger.info("Registry image: [%s]", [registry_image.name()])

        artifact = Artifact(
            namespace=namespace,
            version=version
        )

        for name, manifest in sorted(storage.get_manifests().items()):
            tag = registry_image.push_manifest(
                name=name,
                tag=artifact.id(),
                manifest=manifest
            )
            logger.info("Push [%s]: completed", [tag])
