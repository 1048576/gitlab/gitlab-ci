import os
import os.path
import urllib.parse

from argparse import Namespace
from typing import Dict
from typing import List
from typing import Tuple

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.command import Address
from cu.core.command import Command
from cu.core.exception.expected import ExpectedException
from cu.core.logger import Logger
from cu.core.registry import Config
from cu.core.registry import Manifest
from cu.core.validator import Validators
from cu.storage.impl import CiUtilsStorage
from cu.storage.impl import CiUtilsStorageFactory


class ImageBuildCommand():
    storage_factory: CiUtilsStorageFactory

    arg_target: ArgPositionalArgument

    flag_kaniko_address: ArgFlagArgument
    flag_image_build_context: ArgFlagArgument
    flag_image_build_file: ArgFlagArgument

    flag_http_proxy: ArgFlagArgument
    flag_https_proxy: ArgFlagArgument
    flag_no_proxy: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.storage_factory = CiUtilsStorageFactory(parser)

        self.arg_target = parser.add_positional_argument(
            metavar="target",
            help="Target"
        )

        self.flag_kaniko_address = parser.add_flag_argument(
            flag_name="kaniko-address",
            env_name="CU_KANIKO_ADDRESS"
        )
        self.flag_image_build_context = parser.add_flag_argument(
            flag_name="image-build-context",
            env_name="CU_IMAGE_BUILD_CONTEXT"
        )
        self.flag_image_build_file = parser.add_flag_argument(
            flag_name="image-build-file",
            env_name="CU_IMAGE_BUILD_FILE"
        )

        self.flag_http_proxy = parser.add_flag_argument(
            flag_name="http-proxy",
            env_name="HTTP_PROXY"
        )
        self.flag_https_proxy = parser.add_flag_argument(
            flag_name="https-proxy",
            env_name="HTTPS_PROXY"
        )
        self.flag_no_proxy = parser.add_flag_argument(
            flag_name="no-proxy",
            env_name="NO_PROXY"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        storage = self.storage_factory.create(ns)

        workspace = storage.get_workspace()
        registry_image = storage.get_registry_image()
        target = self.fetch_target(ns)
        kaniko_address = self.fetch_kaniko_address(ns)
        context = self.fetch_context(ns, workspace)
        build_file = self.fetch_build_file(ns, context)
        build_args = sorted(self.fetch_build_args(storage, ns).items())

        cache_repo = "%s/cache" % (registry_image)
        repo = "%s/%s" % (registry_image, target)
        image_name = "%s:%s-tmp" % (repo, storage.pipeline_id)

        logger.info("Workspace: [%s]" % workspace)
        logger.info("Target: [%s]" % target)
        logger.info("Kaniko address: [%s:%d]" % kaniko_address)
        logger.info("Context: [%s]" % context)
        logger.info("Build file: [%s]" % build_file)
        logger.info("Cache repo: [%s]" % cache_repo)
        logger.info("Image name: [%s]" % image_name)
        for k, v in build_args:
            logger.info("Build arg [%s]: [%s]" % (k, v))

        digest = self.build_image(
            logger=logger,
            kaniko_address=kaniko_address,
            workspace=workspace,
            target=target,
            context=context,
            build_file=build_file,
            cache_repo=cache_repo,
            build_args=build_args,
            image_name=image_name
        )

        manifest = Config().create_registry_image(repo).get_manifest(digest)
        storage.add_manifest(target, manifest)

        logger.info("Layers:")
        manifest_parsed = Manifest.parse(manifest)
        for layer in manifest_parsed.layers:
            logger.info(" - Layer [%s]: %.2f MiB" % (layer.digest, layer.size / 1048576))
        logger.info("Digest [%s]: %.2f MiB" % (digest, manifest_parsed.total_size / 1048576))

    def build_image(
        self,
        logger: Logger,
        kaniko_address: Address,
        workspace: str,
        target: str,
        context: str,
        build_file: str,
        cache_repo: str,
        build_args: List[Tuple[str, str]],
        image_name: str
    ) -> str:
        digest_file = workspace + ".digest-file"

        cmd = Command.remote(kaniko_address, "/kaniko/executor")
        cmd.add("--target").add(target)
        cmd.add("--context").add(context)
        cmd.add("--dockerfile").add(build_file)
        cmd.add("--cache")
        cmd.add("--cache-repo").add(cache_repo)
        cmd.add("--destination").add(image_name)
        cmd.add("--digest-file").add(digest_file)
        cmd.add("--skip-unused-stages")

        for k, v in build_args:
            cmd.add("--build-arg").add("%s='%s'" % (k, v))

        try:
            cmd.call_check(logger)
            with open(digest_file) as f:
                digest = f.read()

            return digest
        except ExpectedException as e:
            raise ExpectedException.wrap("Build image failed", e)

    def fetch_target(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.re_validator(r"[a-z]+(?:\-[a-z]+)*")
        )

        return validator.verify(self.arg_target, ns)

    def fetch_kaniko_address(self, ns: Namespace) -> Tuple[str, int]:
        validator = ArgArgumentValidator(
            Validators.re_validator(
                r"(?:[a-z][a-z0-9]+)((\.|-)[a-z][a-z0-9]+)*\:[1-9][0-9]+"
            )
        )
        address = validator.verify(self.flag_kaniko_address, ns)
        host, port = tuple(address.split(":"))

        return (host, int(port))

    def fetch_context(self, ns: Namespace, workspace: str) -> str:
        validator = ArgArgumentValidator(
            Validators.dir_path_validator()
        )
        context = validator.verify(self.flag_image_build_context, ns)
        context = os.path.join(workspace, context)
        context = os.path.abspath(context) + "/"

        if (not context.startswith(workspace)):
            raise ExpectedException(
                "Image build context [{}] out of workspace [{}]".format(
                    context,
                    workspace
                )
            )

        return context

    def fetch_build_file(self, ns: Namespace, context: str) -> str:
        validator = ArgArgumentValidator(
            Validators.file_path_validator()
        )
        build_file = validator.verify(self.flag_image_build_file, ns)
        build_file = os.path.join(context, build_file)
        build_file = os.path.abspath(build_file)

        if (not build_file.startswith(context)):
            raise ExpectedException(
                "Image build file [{}] out of context [{}]".format(
                    build_file,
                    context
                )
            )

        return build_file

    def fetch_build_args(
        self,
        storage: CiUtilsStorage,
        ns: Namespace
    ) -> Dict[str, str]:
        build_args: Dict[str, str] = storage.get_args()

        java_tool_options = ""

        http_proxy = self.flag_http_proxy.value(ns)
        if (isinstance(http_proxy, str) and (http_proxy != "")):
            build_args["HTTP_PROXY"] = http_proxy
            build_args["http_proxy"] = http_proxy
            url_parsed = urllib.parse.urlparse(http_proxy)
            java_tool_options += " -Dhttp.proxyHost=%s" % url_parsed.hostname
            if (url_parsed.port is not None):
                java_tool_options += " -Dhttp.proxyPort=%d" % url_parsed.port

        https_proxy = self.flag_https_proxy.value(ns)
        if (isinstance(https_proxy, str) and (https_proxy != "")):
            build_args["HTTPS_PROXY"] = https_proxy
            build_args["https_proxy"] = https_proxy
            url_parsed = urllib.parse.urlparse(https_proxy)
            java_tool_options += " -Dhttps.proxyHost=%s" % url_parsed.hostname
            if (url_parsed.port is not None):
                java_tool_options += " -Dhttps.proxyPort=%d" % url_parsed.port

        no_proxy = self.flag_no_proxy.value(ns)
        if (isinstance(no_proxy, str) and (no_proxy != "")):
            build_args["NO_PROXY"] = no_proxy
            build_args["no_proxy"] = no_proxy
            java_tool_options += " -Dhttp.nonProxyHosts=" + no_proxy.replace(",", "|")

        if (java_tool_options != ""):
            build_args["JAVA_TOOL_OPTIONS"] = java_tool_options[1:]

        return build_args
