from argparse import Namespace

from cu.cli_cmd import CliCommand
from cu.command.image.build_cmd import ImageBuildCommand
from cu.command.image.push_cmd import ImagePushCommand
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger


class ImageCommand(CliCommand):
    arg_cmd: ArgPositionalArgument
    cmd_build: ImageBuildCommand
    cmd_push: ImagePushCommand

    def __init__(self, parser: ArgParser) -> None:
        self.arg_cmd, subparsers = parser.add_subparsers(
            metavar="IMAGE-COMMAND"
        )

        self.cmd_build = ImageBuildCommand(
            subparsers.add_parser(name="build", help="Build image")
        )

        self.cmd_push = ImagePushCommand(
            subparsers.add_parser(name="push", help="Push images")
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        image_command = self.arg_cmd.value(ns)
        if (image_command == "build"):
            self.cmd_build.run(logger, ns)
        elif (image_command == "push"):
            self.cmd_push.run(logger, ns)
        else:
            raise ExpectedException(
                msg_template="Unknown image command [%s]",
                msg_vars=[image_command]
            )
