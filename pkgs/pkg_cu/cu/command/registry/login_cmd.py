from argparse import Namespace
from typing import Tuple

from cu.core.arg.argument import ArgArgumentValidator
from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.parser import ArgParser
from cu.core.errors import ExpectedException
from cu.core.logger import Logger
from cu.core.registry import Config
from cu.core.registry import Registry
from cu.core.validator import Validators


class RegistryLoginCommand():
    flag_registry_address: ArgFlagArgument
    flag_registry_user: ArgFlagArgument
    flag_registry_password: ArgFlagArgument

    def __init__(self, parser: ArgParser) -> None:
        self.flag_registry_address = parser.add_flag_argument(
            flag_name="registry",
            env_name="CI_REGISTRY"
        )
        self.flag_registry_user = parser.add_flag_argument(
            flag_name="registry-user",
            env_name="CI_REGISTRY_USER"
        )
        self.flag_registry_password = parser.add_flag_argument(
            flag_name="registry-password",
            env_name="CI_REGISTRY_PASSWORD"
        )

    def run(self, logger: Logger, ns: Namespace) -> None:
        address = self.fetch_address(ns)
        user, password = self.fetch_auth(ns)
        registry = Registry("https", address, user, password)

        logger.info("Registry: [%s]", [registry.address])
        logger.info("Registry user: [%s]", [user])

        try:
            registry.version()
            config = Config()
            config.add_registry(registry)
            config.save()
            logger.info("Login succeeded")
        except ExpectedException as e:
            raise e.wrap("Cannot login")

    def fetch_address(self, ns: Namespace) -> str:
        validator = ArgArgumentValidator(
            Validators.re_validator(r"[a-z]+(?:\.[a-z]+)*")
        )

        return validator.verify(self.flag_registry_address, ns)

    def fetch_auth(self, ns: Namespace) -> Tuple[str, str]:
        validator = ArgArgumentValidator(
            Validators.str_validator()
        )

        return (
            validator.verify(self.flag_registry_user, ns),
            validator.verify(self.flag_registry_password, ns)
        )
