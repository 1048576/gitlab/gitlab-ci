from __future__ import annotations

import json
import os
import urllib.parse

from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import cast

import cu.core.http.requests as requests

from cu.core.errors import ExpectedException
from cu.core.exception.unexpected import UnexpectedException
from cu.core.http.requests import HTTPBasicAuth
from cu.core.http.utils import QueryBuilder
from cu.core.http.utils import WWWAuthenticate


class Registry:
    scheme: str
    address: str
    username: str
    password: str
    headers: Dict[str, str]

    def __init__(
        self,
        scheme: str,
        address: str,
        username: str,
        password: str
    ) -> None:
        self.scheme = scheme
        self.address = address
        self.username = username
        self.password = password
        self.headers = {}

    def version(self) -> None:
        self.execute("GET", "/")

    def execute(
        self,
        method: str,
        path: str,
        content: Optional[object] = None,
        headers: Dict[str, str] = {}
    ) -> bytes:
        url = "%s://%s/v2%s" % (self.scheme, self.address, path)
        response = requests.send(
            method=method,
            url=url,
            headers={**self.headers, **headers},
            json=content
        )

        if (response.status_code == 401):
            www_authenticate_text = response.headers.get("www-authenticate")
            if (www_authenticate_text is not None):
                token = self.auth(www_authenticate_text)
                self.headers = {
                    "Authorization": "Bearer " + token
                }
                response = requests.send(
                    method=method,
                    url=url,
                    headers={**self.headers, **headers},
                    json=content
                )

        if (response.status_code in (200, 201)):
            return response.content
        else:
            raise ExpectedException("Status code [%s]", [response.status_code])

    def auth(self, www_authenticate_text: str) -> str:
        www_authenticate = WWWAuthenticate.parse(www_authenticate_text)
        if (www_authenticate.scheme != "Bearer"):
            raise ExpectedException(
                msg_template="Unsupported authentication scheme: [%s]",
                msg_vars=[www_authenticate.scheme]
            )
        query = QueryBuilder(www_authenticate.options).build()
        url = "%s?%s" % (www_authenticate.realm, query)
        response = requests.send(
            method="GET",
            url=url,
            auth=HTTPBasicAuth(self.username, self.password)
        )
        if (response.status_code == 200):
            return json.loads(response.content).pop("token")
        else:
            raise ExpectedException("Unauthorized access")


class ManifestLayer:
    digest: str
    size: int

    def __init__(self, digest: str, size: int) -> None:
        self.digest = digest
        self.size = size


class Manifest:
    MANIFEST_MEDIA_TYPE = "application/vnd.docker.distribution.manifest.v2+json"  # noqa: E501

    layers: List[ManifestLayer]
    total_size: int

    @staticmethod
    def parse(unverified_manifest: Dict[str, object]) -> Manifest:
        media_type = unverified_manifest.get("mediaType")
        if (media_type != Manifest.MANIFEST_MEDIA_TYPE):
            raise UnexpectedException("Invalid media type")

        unverified_layers = unverified_manifest.get("layers")
        if (isinstance(unverified_layers, list)):
            verified_layers = cast(List[object], unverified_layers)
        else:
            raise UnexpectedException("Invalid layers")

        layers: List[ManifestLayer] = []
        for unverified_layer in verified_layers:
            if (isinstance(unverified_layer, dict)):
                verified_layer = cast(Dict[str, object], unverified_layer)
            else:
                raise UnexpectedException("Invalid layer")
            digest = verified_layer.get("digest")
            size = verified_layer.get("size")
            if (not isinstance(digest, str)):
                raise UnexpectedException("Invalid layer digest")
            if (not isinstance(size, int)):
                raise UnexpectedException("Invalid layer size")
            layers.append(ManifestLayer(digest, size))

        return Manifest(layers)

    def __init__(self, layers: List[ManifestLayer]) -> None:
        self.layers = layers
        self.total_size = 0

        for layer in self.layers:
            self.total_size += layer.size


class RegistryImage:
    registry: Registry
    repo: str

    def __init__(self, registry: Registry, repo: str) -> None:
        self.registry = registry
        self.repo = repo

    def get_manifest(self, digest: str) -> Dict[str, object]:
        path = "%s/manifests/%s" % (self.repo, urllib.parse.quote_plus(digest))
        return json.loads(
            self.registry.execute("GET", path)
        )

    def push_manifest(self, name: str, tag: str, manifest: object) -> str:
        quote_tag = urllib.parse.quote_plus(tag)
        path = "%s/%s/manifests/%s" % (self.repo, name, quote_tag)
        self.registry.execute(
            method="PUT",
            path=path,
            headers={
                "Content-Type": Manifest.MANIFEST_MEDIA_TYPE
            },
            content=manifest
        )

        return "%s/%s:%s" % (self.name(), name, tag)

    def name(self) -> str:
        return self.registry.address + self.repo


class Config:
    config_dir_path: str
    config_path: str
    config: Dict[str, Any]
    auths: Dict[str, Dict[str, str]]

    def __init__(self) -> None:
        home_dir = os.environ.get("HOME", "/root/")
        self.config_dir_path = os.path.abspath(
            os.path.join(home_dir, "./.docker/")
        )
        self.config_path = os.path.abspath(
            os.path.join(self.config_dir_path, "config.json")
        )

        if (os.path.isfile(self.config_path)):
            with open(self.config_path, "r") as f:
                self.config = json.loads(f.read())
            auths: Optional[Dict[str, Any]] = self.config.get("auths")
            if (auths is None):
                self.auths = {}
                self.config["auths"] = self.auths
            else:
                self.auths = auths
        else:
            self.config = {}
            self.auths = {}
            self.config["auths"] = self.auths

    def save(self) -> None:
        os.makedirs(self.config_dir_path, exist_ok=True)
        with open(self.config_path, "w") as f:
            f.write(json.dumps(self.config) + "\n")

    def add_registry(self, registry: Registry) -> None:
        self.auths[registry.address] = {
            "username": registry.username,
            "password": registry.password
        }

    def create_registry_image(self, registry_image_name: str) -> RegistryImage:
        for registry_name, auth in self.auths.items():
            if registry_image_name.startswith(registry_name):
                username = auth.get("username")
                password = auth.get("password")
                if ((username is not None) and (password is not None)):
                    return RegistryImage(
                        Registry("https", registry_name, username, password),
                        registry_image_name[len(registry_name):]
                    )

        raise ExpectedException(
            msg_template="Registry image auth not found: [%s]",
            msg_vars=[registry_image_name]
        )
