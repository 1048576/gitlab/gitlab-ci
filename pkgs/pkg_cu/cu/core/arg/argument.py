from __future__ import annotations

from argparse import Namespace
from typing import Callable
from typing import Generic
from typing import List
from typing import Optional
from typing import TypeVar

from cu.core.errors import ExpectedException
from cu.core.validator import Validator

T = TypeVar("T")


class ArgArgument(Generic[T]):
    id: str
    metavar: str

    def __init__(self, id: str, metavar: str) -> None:
        self.id = id
        self.metavar = metavar

    def value(self, ns: Namespace) -> T:
        return getattr(ns, self.id)

    def value_as_object(
        self,
        ns: Namespace,
        default: Optional[Callable[[], object]] = None
    ) -> object:
        value = getattr(ns, self.id)

        if (value is not None):
            return value

        if (default is not None):
            return default()

        raise ExpectedException(
            msg_template="Arg [%s] is required",
            msg_vars=[self.metavar]
        )

    def value_as_text(
        self,
        ns: Namespace,
        default: Optional[Callable[[], str]] = None
    ) -> str:
        value = self.value_as_object(ns, default)

        if (isinstance(value, str)):
            return value
        else:
            raise Exception("Unreachable state")

    def value_as_bool(
        self,
        ns: Namespace,
        default: Optional[Callable[[], bool]] = None
    ) -> bool:
        if (default is None):
            value = self.value_as_object(ns)
        else:
            value = self.value_as_object(ns, lambda: None)

            if (value is None):
                return default()

        if (value == "true"):
            return True
        elif (value == "false"):
            return False
        else:
            raise ExpectedException(
                msg_template="[%s] instead of bool",
                msg_vars=[value]
            )

    def is_defined(self, ns: Namespace) -> bool:
        return self.value(ns) is not None


ArgPositionalArgument = ArgArgument[str]
ArgFlagArgument = ArgArgument[Optional[str]]
ArgMultiFlagArgument = ArgArgument[List[str]]

X = TypeVar("X")
Y = TypeVar("Y")


class ArgArgumentValidator(Generic[X, Y]):
    validator: Validator[X, Y]

    def __init__(self, validator: Validator[X, Y]) -> None:
        self.validator = validator

    def verify(self, arg: ArgArgument[X], ns: Namespace) -> Y:
        value = arg.value(ns)

        try:
            return self.validator.verify(value)
        except ExpectedException as e:
            raise e.wrap(
                title_template="Invalid [%s]",
                title_vars=[arg.metavar]
            )


class ArgMultiArgumentValidator(Generic[X, Y]):
    validator: Validator[X, Y]

    def __init__(self, validator: Validator[X, Y]) -> None:
        self.validator = validator

    def verify(self, arg: ArgArgument[List[X]], ns: Namespace) -> List[Y]:
        unverified_values = arg.value(ns)

        try:
            values: List[Y] = []

            for unverified_value in unverified_values:
                values.append(self.validator.verify(unverified_value))

            return values
        except ExpectedException as e:
            raise e.wrap(
                title_template="Invalid [%s]",
                title_vars=[arg.metavar]
            )
