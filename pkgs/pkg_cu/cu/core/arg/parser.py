from __future__ import annotations

import os

from argparse import ArgumentParser
from argparse import Namespace
from typing import Optional
from typing import Tuple

from cu.core.arg.argument import ArgFlagArgument
from cu.core.arg.argument import ArgMultiFlagArgument
from cu.core.arg.argument import ArgPositionalArgument
from cu.core.errors import ExpectedException


class ArgSubParsers:
    def add_parser(self, name: str, help: str) -> ArgParser: ...


class ArgParser:
    origin: ArgumentParser

    @staticmethod
    def create() -> ArgParser:
        return ArgParser(ArgumentParser())

    def __init__(self, origin: ArgumentParser) -> None:
        self.origin = origin

    def parse(self) -> Namespace:
        namespace = self.origin.parse_args()

        for k, v in vars(namespace).items():
            if (not k.startswith("F:")):
                continue

            flag_name, env_name = tuple(k[2:].split(":"))
            env_value = os.environ.get(env_name)

            if (v is None):
                setattr(namespace, k, env_value)
            elif (env_value is not None):
                formatted_message = (
                    "Conflicting environment variable [%s] is shadowed by "
                    "[--%s] command-line flag " +
                    "(either unset environment variable or disable flag)"
                )
                raise ExpectedException(formatted_message % (env_name, flag_name))

        return namespace

    def add_subparsers(
        self,
        metavar: str
    ) -> Tuple[ArgPositionalArgument, ArgSubParsers]:
        id = "A:" + metavar

        class ArgSubParsersImpl(ArgSubParsers):
            def __init__(
                self,
                parent: ArgParser,
                id: str,
                metavar: str
            ) -> None:
                self.parent = parent
                self.parsers = parent.origin.add_subparsers(
                    metavar=metavar,
                    required=True,
                    dest=id
                )

            def add_parser(self, name: str, help: str) -> ArgParser:
                return ArgParser(
                    origin=self.parsers.add_parser(name, help=help)
                )

        return (ArgPositionalArgument(id, metavar), ArgSubParsersImpl(self, id, metavar))

    def add_positional_argument(self, metavar: str, help: str) -> ArgPositionalArgument:
        id = "A:" + metavar

        self.origin.add_argument(
            metavar=metavar,
            dest=id,
            help=help
        )

        return ArgPositionalArgument(id, metavar)

    def add_flag_argument(self, flag_name: str, env_name: str, help: Optional[str] = None) -> ArgFlagArgument:
        id = "F:%s:%s" % (flag_name, env_name)

        self.origin.add_argument(
            "--" + flag_name,
            metavar=env_name,
            dest=id,
            help=help
        )

        return ArgFlagArgument(id, env_name)

    def add_multi_flag_argument(self, flag_name: str, metavar: str, help: Optional[str] = None) -> ArgMultiFlagArgument:
        id = "FM:%s:%s" % (flag_name, metavar)

        self.origin.add_argument(
            "--" + flag_name,
            metavar=metavar,
            dest=id,
            action="append",
            help=help
        )

        return ArgMultiFlagArgument(id, metavar)
