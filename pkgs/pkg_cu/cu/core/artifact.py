class Artifact:
    namespace: str
    version: str

    def __init__(self, namespace: str, version: str) -> None:
        self.namespace = namespace
        self.version = version

    def id(self) -> str:
        return self.version + "-" + self.namespace
