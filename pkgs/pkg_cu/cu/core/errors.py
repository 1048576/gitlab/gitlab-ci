from __future__ import annotations

from typing import List


class ExpectedException(Exception):
    @staticmethod
    def create_and_wrap(
        msg_template: str,
        title_template: str,
        title_vars: List[object] = []
    ) -> ExpectedException:
        e = ExpectedException(msg_template)

        return e.wrap(title_template, title_vars)

    def __init__(
        self,
        msg_template: str,
        msg_vars: List[object] = []
    ) -> None:
        super().__init__(msg_template % tuple(msg_vars))

    def wrap(
        self,
        title_template: str,
        title_vars: List[object] = []
    ) -> ExpectedException:
        title_rendered = title_template % tuple(title_vars)
        body = str(self).replace("\n", "\n  ")

        return ExpectedException("%s:\n  %s" % (title_rendered, body))
