import sys

from typing import List

from cu.core.errors import ExpectedException


class Logger:
    def info(
        self,
        text_template: str,
        text_vars: List[object] = [],
        colored: bool = True
    ) -> None:
        text_rendered = text_template % tuple(text_vars)
        if (colored):
            print("\033[36m" + text_rendered + "\033[39m")
        else:
            print(text_rendered)
        sys.stdout.flush()

    def error(self, e: ExpectedException) -> None:
        print("\033[1;31m" + str(e) + "\033[39m")
        sys.stdout.flush()
