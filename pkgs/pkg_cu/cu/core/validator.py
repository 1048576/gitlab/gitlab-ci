import re

from typing import Any
from typing import Generic
from typing import Pattern
from typing import TypeVar

import cu.core.path_utils as path_utils

from cu.core.errors import ExpectedException

X = TypeVar("X")
Y = TypeVar("Y")
Z = TypeVar("Z")


class Validator(Generic[X, Y]):
    def verify(self, value: X) -> Y: ...


class PairValidator(Generic[X, Y, Z], Validator[X, Z]):
    first: Validator[X, Y]
    second: Validator[Y, Z]

    def __init__(self, first: Validator[X, Y], second: Validator[Y, Z]) -> None:
        self.first = first
        self.second = second

    def verify(self, value: X) -> Z:
        y = self.first.verify(value)

        return self.second.verify(y)


class StrValidator(Validator[object, str]):
    def verify(self, value: object) -> str:
        if isinstance(value, str):
            return value

        if (value is None):
            raise ExpectedException("None instead of text")

        raise ExpectedException("[%s] instead of text", [type(value).__name__])


class ReValidator(Validator[str, str]):
    compiled_pattern: Pattern[str]

    def __init__(self, pattern: str) -> None:
        self.compiled_pattern = re.compile("^" + pattern + "$")

    def verify(self, value: str) -> str:
        if (self.compiled_pattern.match(value) is None):
            raise ExpectedException("Value [%s] not match [%s]", [value, self.compiled_pattern.pattern])

        return value


class DirPathValidator(Validator[str, str]):
    def verify(self, value: str) -> str:
        if (not path_utils.norm(value).endswith("/")):
            raise ExpectedException("Invalid dir path [%s]", [value])

        return value


class FilePathValidator(Validator[str, str]):
    def verify(self, value: str) -> str:
        if path_utils.norm(value).endswith("/"):
            raise ExpectedException("Invalid file path [%s]", [value])

        return value


class PathValidator(Validator[str, str]):
    def verify(self, value: str) -> str:
        return path_utils.norm(value)


class Validators:
    @staticmethod
    def str_validator() -> Validator[Any, str]:
        return StrValidator()

    @staticmethod
    def path_validator_simplified() -> Validator[str, str]:
        return PathValidator()

    @staticmethod
    def path_validator() -> Validator[Any, str]:
        return PairValidator(
            Validators.str_validator(),
            Validators.path_validator_simplified()
        )

    @staticmethod
    def dir_path_validator_simplified() -> Validator[str, str]:
        return DirPathValidator()

    @staticmethod
    def dir_path_validator() -> Validator[Any, str]:
        return PairValidator(
            Validators.str_validator(),
            Validators.dir_path_validator_simplified()
        )

    @staticmethod
    def file_path_validator_simplified() -> Validator[str, str]:
        return FilePathValidator()

    @staticmethod
    def file_path_validator() -> Validator[Any, str]:
        return PairValidator(
            Validators.str_validator(),
            Validators.file_path_validator_simplified()
        )

    @staticmethod
    def re_validator_simplified(pattern: str) -> Validator[str, str]:
        return ReValidator(pattern)

    @staticmethod
    def re_validator(pattern: str) -> Validator[Any, str]:
        return PairValidator(
            Validators.str_validator(),
            Validators.re_validator_simplified(pattern)
        )
