#!/bin/sh

set -o errexit
set -o nounset
set -o pipefail

function _sh {
    local command=$1
    echo -e "\e[1;34m$(pwd)\e[32m\$ ${command}\e[39m"
    sh -c "${command}"
}

# Pre install
_sh "apk add --no-cache --virtual .build-deps gcc musl-dev libffi-dev"

# Install ansible
_sh "pip3 install --no-cache-dir ansible==${BUILD_ARG_ANSIBLE_VERSION}"
echo -e "\033[0;32mInstalled ansible:\n$(ansible --version)\033[0m"

# Post install
_sh "apk del .build-deps"
_sh "rm $0"
